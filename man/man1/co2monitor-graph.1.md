% co2monitor-graph(1) | co2monitor-graph manpage

NAME
====

**co2monitor-graph** - shows a graph of the current CO2 measurement


SYNOPSIS
========

**co2monitor-graph**


AUTHOR
======

Yann Büchau <nobodyinperson@gmx.de>


SEE ALSO
========

the service **co2monitor-service(1)**, the gui **co2monitor(1)**

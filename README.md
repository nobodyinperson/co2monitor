# co2monitor

Automatic data logging for AirCO2ntrol mini USB co2 measurement devices.

**Hint**: This application is compatible with Raspbian.

**Note**: This application is in development state.

![co2monitor svg](https://gitlab.com/nobodyinperson/co2monitor/uploads/ce661cf747335ac216e1d2ded05526f6/co2monitor.png)

## Background

Hendryk Plötz reverse-engineered the usb protocol on [hackaday.io](https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor).
I wanted to have an easy-to-use plug'n'play data logging solution for Debian/Ubuntu systems.
That's what **co2monitor** is.

## Installation

### Debian package

There are ready-to-use debian packages on the [tags
page](https://gitlab.com/nobodyinperson/co2monitor/tags), you may download the
latest release there.

To build a debian package from the repository, run ```dpkg-buildpackage -us
-uc``` (options mean without signing) from the repository root.  There will be a
```co2monitor_*.deb``` one folder layer above.

Install the debian package via ```sudo dpkg -i co2monitor_*.deb```.
Older versions of **co2monitor** will automatically be removed.

Remove **co2monitor** from your system via ```sudo apt-get remove co2monitor```.

### My own APT repository

For automatic updates, you may use my [apt
repository](http://apt.nobodyinperson.de). If you do, install **co2monitor**
like any other package via ```sudo apt-get update && sudo apt-get
install co2monitor```

Remove **co2monitor** from your system via ```sudo apt-get remove co2monitor```.

### Manual Installation

You can also install **co2monitor** by hand. You may want to do that if you are
using a distribution that does not utilise the `.deb` software package format.
You will still want to install it with root privileges because of the `systemd`
and/or `udev`-stuff.

```bash
# clone the repository
git clone https://gitlab.com/nobodyinperson/co2monitor
cd co2monitor
git submodule update --init # update submodules
./configure # see ./configure -h for help on additional options
make
# check what this would do with `make -n install`
make install # do this as root
```

Errors when running `./configure` are most likely connected to requirements not
being installed. It will tell you what's missing.

To uninstall **co2monitor**, run from the git repository root:

```bash
make uninstall
```

To upgrade to the latest development version, run from the repository root:

```bash
# check what this would do with `make -n uninstall`
make uninstall # do this as root
git pull
./configure
make
make install # do this as root
```

## Important files

|           file                    |                  purpose              |
|-----------------------------------|---------------------------------------|
| ```etc/co2monitor/service.conf``` | co2monitor service configuration file |
| ```bin/co2monitor-service```      |  the co2monitor service executable    |
| ```bin/co2monitor```              | the co2monitor application executable |
| ```var/lib/co2monitor/data/*```   | standard folder for logged co2 data   |

## Special thanks

- Hendryk Plötz on [hackaday.io](https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor) for the device interaction
- Mike Kazantsev on his blog on [fraggod.net](http://blog.fraggod.net/2012/06/16/proper-ish-way-to-start-long-running-systemd-service-on-udev-event-device-hotplug.html) for the systemd integration
- Ascot on [StackOverflow.com](http://stackoverflow.com/a/26457317/5433146) for a workaround on ```signal.signal(signal, handler)``` when using a ```GLib.MainLoop```
- don_crissti on [StackOveflow.com](http://unix.stackexchange.com/a/203678) for
  getting a list of dbus objects
